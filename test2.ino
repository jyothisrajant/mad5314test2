// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

// Make an InternetButton object
InternetButton button = InternetButton();
void setup() {
 button.begin();


  Particle.function("smileLights", turnSmileOn);


}

void loop() {

}

int turnSmileOn(String cmd){
     int time = cmd.toInt();
     if(time==1){
         button.allLedsOff();
         button.ledOn(1,0, 255, 0);
         button.ledOn(11,0, 255, 0);
         button.ledOn(3,0, 255, 0);
         button.ledOn(4,0, 255, 0);
         button.ledOn(5,0, 255, 0);
         button.ledOn(6,0, 255, 0);
         button.ledOn(7,0, 255, 0);
         button.ledOn(8,0, 255, 0);
         button.ledOn(9,0, 255, 0);
     }else if(time==5){
        button.allLedsOff();
        button.ledOn(1,0, 255, 0);
        button.ledOn(11,0, 255, 0);
        button.ledOn(4,0, 255, 0);
        button.ledOn(5,0, 255, 0);
        button.ledOn(6,0, 255, 0);
        button.ledOn(7,0, 255, 0);
        button.ledOn(8,0, 255, 0);
     }else if(time==9){
         button.allLedsOff();
         button.ledOn(1,0, 255, 0);
         button.ledOn(11,0, 255, 0);
         button.ledOn(5,0, 255, 0);
         button.ledOn(6,0, 255, 0);
         button.ledOn(7,0, 255, 0);
     }else if(time==13){
         button.allLedsOff();
         button.ledOn(1,0, 255, 0);
         button.ledOn(11,0, 255, 0);
         button.ledOn(6,0, 255, 0);
     }else if(time==17){
         button.allLedsOff();
         button.ledOn(1,0, 255, 0);
         button.ledOn(11,0, 255, 0);
     }else if(time==20){
         button.allLedsOff();
         button.ledOn(1,255, 0, 0);
         button.ledOn(11,255, 0, 0);
         button.ledOn(7,255, 0, 0);
         button.ledOn(5,255, 0, 0);
     }
}
