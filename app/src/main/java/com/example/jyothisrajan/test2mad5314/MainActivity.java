package com.example.jyothisrajan.test2mad5314;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    TextView slowText,timeText;
    Button startBtn;
    SeekBar seekBar;
    // MARK: Debug info
    private final String TAG="Mad5314Test2";

    // MARK: Particle Account Info
    private final String PARTICLE_USERNAME = "jyothisrajant@gmail.com";
    private final String PARTICLE_PASSWORD = "jyrinternet";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "1c002a001247363333343437";
    // MARK: Particle device
    private ParticleDevice mDevice;
    int counter=0;
    CountDownTimer countDownTimer1;
    int timerSpeed=1000;
    boolean timerCancel=true;
    boolean timerFirstTime=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();
        setContentView(R.layout.activity_main);
        slowText=findViewById(R.id.slow_text);
        timeText=findViewById(R.id.time_text);
        startBtn=findViewById(R.id.start_btn);
        seekBar=findViewById(R.id.seekBar);
        seekBar.setMax(50000);
        seekBar.setProgress(1000);


        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                counter=0;

                countDownTimer1=new CountDownTimer(500000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                        if(counter<=20) {

                            timeText.setText(String.valueOf(counter));
                            showSmile(counter);
                            counter++;
                        }
                        }


                    @Override
                    public void onFinish() {

                    }
                }.start();


                }


        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                if(progress==0){
                    slowText.setText("Time slow down by: " + 1);
                }else {
                    slowText.setText("Time slow down by: " + String.valueOf(progress / 1000));
                }
                Log.d(TAG, "timer speed!!"+progress);
                //timerSpeed=progress;




            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                    countDownTimer1.cancel();



            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int speed;
                if(seekBar.getProgress()==0){
                    speed=1000;
                }else {
                    speed = seekBar.getProgress();
                }
                countDownTimer1=new CountDownTimer(300000, speed) {

                    public void onTick(long millisUntilFinished) {
                        if(counter<=20) {

                                timeText.setText(String.valueOf(counter));


                            showSmile(counter);
                            counter++;
                        }
                    }

                    public void onFinish() {

                    }
                }.start();
            }
        });

    }

    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;

            }

            @Override
            public void onSuccess(Object o) {

                Log.d(TAG, "Successfully got device from Cloud");

                // if you get the device, then go subscribe to events

            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }
    public void showSmile(int counterTime) {

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                // put your logic here to talk to the particle
                // --------------------------------------------
                List<String> functionParameters = new ArrayList<String>();

                functionParameters.add(""+counterTime);

                try {
                    Log.d(TAG, "Smile");
                    mDevice.callFunction("smileLights", functionParameters);

                } catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }


                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                // put your success message here
                Log.d(TAG, "Success");

            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                // put your error handling code here
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

}
